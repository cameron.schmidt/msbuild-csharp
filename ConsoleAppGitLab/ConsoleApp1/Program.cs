﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            var iAmTrue = true; 
            if (iAmTrue) 
            { 
                Console.WriteLine("true"); 
            } 
            else 
            { 
                Console.WriteLine("false"); 
            }
            Console.ReadKey();
        }        
        public static bool AlwaysReturnsTrue() 
        { 
            return true; 
        }        
        public static object Passthrough(object obj) 
        { 
            return obj; 
        }
    }
}
